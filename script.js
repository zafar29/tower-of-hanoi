const rightTower = document.getElementById("poleR");
const centerTower = document.getElementById("poleC");
const leftTower = document.getElementById("poleL");
const redDisk = document.getElementById("redDisk");
const blueDisk = document.getElementById("blueDisk");
const greenDisk = document.getElementById("greenDisk");
const gameBoard = document.getElementById("gameBoard");

let towerClicked = leftTower;

gameBoard.addEventListener('click', function (event){
    if (checkDiskOrder(towerClicked,event.target) === "Illegal Move"){return}   //check rules
    if (event.target  === event.currentTarget){towerClicked  = gameBoard}       //gameboard click deselects tower
    moveDisk(towerClicked,event.target);
    checkWinCondition();
    towerClicked  = event.target;
});

function checkDiskOrder (sourceTower,destinationTower){
    if(destinationTower.childElementCount < 1){return}
    if (Number(sourceTower.lastElementChild.dataset.value) > Number(destinationTower.lastElementChild.dataset.value))
    {return "Illegal Move"}
}

function moveDisk (sourceTower,destinationTower) {
    if((sourceTower === gameBoard) || destinationTower === gameBoard){return}
    let disk  = sourceTower.lastElementChild;
    destinationTower.appendChild(disk);
}

function checkWinCondition (){
    if (rightTower.childElementCount === 3){
        window.alert("You Win!")
    }
}